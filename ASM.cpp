﻿#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:4996)

#include "stdio.h"
#include <iostream>
#include <vector>
#include <string>

using namespace std;

void asmr(char* arr, int siz, int n) {
char format[] = "%c\n";
int count, tp1, tp2=0;
__asm {
cmp		siz, 0   //Проверка на пустую строку
je      TADA
mov		tp1,0    //tp1 (temp1) это переменная, которая выполняет роль индекса i
mov		ecx, siz //кол-во итераций FLOOP
	FLOOP:       //основной цикл
cmp		esi,siz	 //проверка, не дошли ли мы до конца массива
je      KOSTIL				
mov		count, 1 //count - сколько раз встречается этот чар
SLOOP :			 //самопальный второй цикл (смотрите функцию ниже)
mov		edx, tp1 //это i
cmp		edx, tp2 //сравниваем i и k
je		TLOOP    //если равны делаем break и идём на третий цикл
mov		ebx, arr 
mov		esi, tp1
movsx   eax, [ebx][esi] //закинули arr[i]
mov		ebx, arr        
mov		esi, tp2
movsx   ebx, [ebx][esi] //закинули arr[k]
cmp		eax, ebx        //сравниваем чары
je		KOSTIL
add		tp2, 1          //двигаем k
jmp		SLOOP           //зацикливаем? Конечно!
	TLOOP:				//подготовка к третьему циклу, проверки
mov     esi, tp1
mov		ebx, tp1
add		ebx, 1//записали i+1
mov		eax, siz
cmp		ebx, eax//сравниваем не равно ли j размеру массива
mov		ebx, arr
movsx   eax, [ebx][esi]
je	FINISH
	POINT:				//сам третий цикл, тут проверки индексов, и тп
cmp		esi, siz
je		FINISH  
mov		ebx, arr
movsx   ebx, [ebx][esi+1]
inc		esi
cmp		ebx, eax
je		COUNT
jmp		POINT
	COUNT: 
add		count, 1//счётчик для 3 цикла
jmp		POINT
	FINISH:     //выход с 3 цикла, сравниваем count с нужным числом повторений
mov		esi, count
cmp		esi, n
je PRINT        //принтуем, если совпало

	KOSTIL:     //метка для раннего выхода с циклов. Специфическое название сохранено ради истории
add		tp1,1
mov		tp2,0
loop	FLOOP
jmp		TADA   //прыжок на завершение
PRINT: //цикл для печатания
push	ecx
push	al     //это закидывает 4 байта, хотя может 2. Почему? 
lea		eax, format
push	eax
call	printf
add		esp, 8
pop		ecx
jmp		KOSTIL
	TADA:    //Работает, наверное
}
}
//ЭТА ФУНКЦИЯ БЫЛА ПРОТОТИПОМ ТОМУ, ЧТО МЫ НАПИСАЛИ
void function(char* array, int size, int n) {
bool flag = true;
int count = 1;
for (int i = 0; i < size; i++) {
count = 1;
for (int k = 0; k < i; k++) {
if (array[i] == array[k]) {
flag = false;
break;
}
}
if (flag == true) {
for (int j = i + 1; j < size; j++) {
if (array[i] == array[j]) {
count++;
}
}
if (count == n) {
cout << array[i];
}
}
flag = true;
}
}

int main()
{
	char x = '1';
	int count=0;
	while (x != 0) {
		cout << "Press 1 to start program; 0 to exit" << endl;
		//cin >> x;
		scanf("%c", &x);
		if (x == '0') {
			break;
		}
		else if (x == '1') {
			cout << "Enter what do you searching for" << endl;
			scanf("%d", &count);
			if (count>=0) {
				std::string str;
				printf("write something\n");
				cin.ignore();
				getline(cin, str);
				char* cstr = new char[str.length() + 1];
				asmr(strcpy(cstr, str.c_str()), str.length(), count);
				delete[] cstr;
				system("pause");
				system("cls");
			}
		}
		
	}
return 0;
}